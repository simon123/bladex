/*
 *      Copyright (c) 2018-2028, Chill Zhuang All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 *  Neither the name of the dreamlu.net developer nor the names of its
 *  contributors may be used to endorse or promote products derived from
 *  this software without specific prior written permission.
 *  Author: Chill 庄骞 (smallchill@163.com)
 */
package org.springblade.oa.service.impl;

import com.baomidou.mybatisplus.core.metadata.IPage;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springblade.core.log.exception.ServiceException;
import org.springblade.core.mp.base.BaseServiceImpl;
import org.springblade.core.secure.utils.AuthUtil;
import org.springblade.core.tool.api.R;
import org.springblade.core.tool.support.Kv;
import org.springblade.core.tool.utils.DateUtil;
import org.springblade.core.tool.utils.Func;
import org.springblade.flow.core.constant.ProcessConstant;
import org.springblade.flow.core.entity.BladeFlow;
import org.springblade.flow.core.feign.IFlowClient;
import org.springblade.flow.core.utils.FlowUtil;
import org.springblade.oa.entity.AttendanceOvertime;
import org.springblade.oa.mapper.AttendanceOvertimeMapper;
import org.springblade.oa.service.IAttendanceOvertimeService;
import org.springblade.oa.vo.AttendanceOvertimeVO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 加班申请表 服务实现类
 *
 * @author BladeX
 * @since 2021-03-05
 */
@Slf4j
@Service
@AllArgsConstructor
public class AttendanceOvertimeServiceImpl extends BaseServiceImpl<AttendanceOvertimeMapper, AttendanceOvertime> implements IAttendanceOvertimeService {

	private final IFlowClient flowClient;

	@Override
	public IPage<AttendanceOvertimeVO> selectAttendanceOvertimePage(IPage<AttendanceOvertimeVO> page, AttendanceOvertimeVO attendanceOvertime) {
		return page.setRecords(baseMapper.selectAttendanceOvertimePage(page, attendanceOvertime));
	}




	@Override
	@Transactional(rollbackFor = Exception.class)
	// @GlobalTransactional
	public boolean startProcess(AttendanceOvertime overTime) {
		String businessTable = FlowUtil.getBusinessTable(ProcessConstant.OVERTIME_KEY);

		if (Func.isEmpty(overTime.getId())) {
			// 保存leave
			overTime.setCreateTime(DateUtil.now());
			save(overTime);

			// 启动流程
			Kv variables = Kv.create()
				.set(ProcessConstant.TASK_VARIABLE_CREATE_USER, AuthUtil.getUserName())
//				.set("taskUser", TaskUtil.getTaskUser(overTime.getTaskUser()))
				.set("days", DateUtil.between(overTime.getStartTime(), overTime.getEndTime()).toDays());

			//将变量传入工作流远程调用接口，开启一个新流程（cloud版为feign，boot版为service）
			R<BladeFlow> result = flowClient.startProcessInstanceById(overTime.getProcessDefinitionId(), FlowUtil.getBusinessKey(businessTable, String.valueOf(overTime.getId())), variables);


			//如果流程启动成功，则将流程主键反向插入流程业务表
			if (result.isSuccess()) {
				log.debug("流程已启动,流程ID:" + result.getData().getProcessInstanceId());
				// 返回流程id写入leave
//				BladeFlow bladeFlow = new BladeFlow();
//				bladeFlow.setProcessInstanceId(result.getData().getProcessInstanceId());
				overTime.setProcessInstanceId(result.getData().getProcessInstanceId());
				updateById(overTime);
			} else {
				throw new ServiceException("开启流程失败");
			}
		} else {
			updateById(overTime);
		}
		return true;
	}

}
