/*
 *      Copyright (c) 2018-2028, Chill Zhuang All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 *  Neither the name of the dreamlu.net developer nor the names of its
 *  contributors may be used to endorse or promote products derived from
 *  this software without specific prior written permission.
 *  Author: Chill 庄骞 (smallchill@163.com)
 */
package org.springblade.oa.entity;

import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import org.springblade.core.mp.base.BaseEntity;
import java.time.LocalDateTime;
import java.util.Date;

import lombok.Data;
import lombok.EqualsAndHashCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import org.springblade.flow.core.entity.FlowEntity;

/**
 * 加班申请表实体类
 *
 * @author BladeX
 * @since 2021-03-05
 */
@Data
@TableName("blade_flw_attendance_overtime")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "AttendanceOvertime对象", description = "加班申请表")
public class AttendanceOvertime   extends FlowEntity  {

	private static final long serialVersionUID = 1L;



	/**
	 * 流程定义id
	 */
	private String processDefinitionId;
	/**
	 * 流程实例id
	 */
	private String processInstanceId;
	/**
	* 申请人
	*/
		@ApiModelProperty(value = "申请人")
		private Long userCode;
	/**
	* 部门编码
	*/
		@ApiModelProperty(value = "部门编码")
		private Long officeCode;
	/**
	* 部门名称
	*/
		@ApiModelProperty(value = "部门名称")
		private String officeName;
	/**
	* 加班人员
	*/
		@ApiModelProperty(value = "加班人员")
		private String userCodes;
	/**
	* 加班人员名称
	*/
		@ApiModelProperty(value = "加班人员名称")
		private String userNames;
	/**
	* 开始时间
	*/
		@ApiModelProperty(value = "开始时间")
		private Date startTime;
	/**
	* 结束时间
	*/
		@ApiModelProperty(value = "结束时间")
		private Date endTime;
	/**
	* 小时数
	*/
		@ApiModelProperty(value = "小时数")
		private BigDecimal hours;
	/**
	* 原因
	*/
		@ApiModelProperty(value = "原因")
		private String reason;
	/**
	* 备注
	*/
		@ApiModelProperty(value = "备注")
		private String remarks;


}
