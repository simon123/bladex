/*
 *      Copyright (c) 2018-2028, Chill Zhuang All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 *  Neither the name of the dreamlu.net developer nor the names of its
 *  contributors may be used to endorse or promote products derived from
 *  this software without specific prior written permission.
 *  Author: Chill 庄骞 (smallchill@163.com)
 */
package org.springblade.project.management.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import lombok.AllArgsConstructor;
import javax.validation.Valid;

import org.springblade.core.mp.support.Condition;
import org.springblade.core.mp.support.Query;
import org.springblade.core.tool.api.R;
import org.springblade.core.tool.utils.Func;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.bind.annotation.RequestParam;
import com.baomidou.mybatisplus.core.metadata.IPage;
import org.springblade.project.management.entity.Info;
import org.springblade.project.management.vo.InfoVO;
import org.springblade.project.management.service.IInfoService;
import org.springblade.core.boot.ctrl.BladeController;

/**
 * 项目基础信息表 控制器
 *
 * @author BladeX
 * @since 2021-04-12
 */
@RestController
@AllArgsConstructor
@RequestMapping("/info")
@Api(value = "项目基础信息表", tags = "项目基础信息表接口")
public class InfoController extends BladeController {

	private final IInfoService infoService;

	/**
	 * 详情
	 */
	@GetMapping("/detail")
	@ApiOperationSupport(order = 1)
	@ApiOperation(value = "详情", notes = "传入info")
	public R<Info> detail(Info info) {
		Info detail = infoService.getOne(Condition.getQueryWrapper(info));
		return R.data(detail);
	}

	/**
	 * 分页 项目基础信息表
	 */
	@GetMapping("/list")
	@ApiOperationSupport(order = 2)
	@ApiOperation(value = "分页", notes = "传入info")
	public R<IPage<Info>> list(Info info, Query query) {
		IPage<Info> pages = infoService.page(Condition.getPage(query), Condition.getQueryWrapper(info));
		return R.data(pages);
	}

	/**
	 * 自定义分页 项目基础信息表
	 */
	@GetMapping("/page")
	@ApiOperationSupport(order = 3)
	@ApiOperation(value = "分页", notes = "传入info")
	public R<IPage<InfoVO>> page(InfoVO info, Query query) {
		IPage<InfoVO> pages = infoService.selectInfoPage(Condition.getPage(query), info);
		return R.data(pages);
	}

	/**
	 * 新增 项目基础信息表
	 */
	@PostMapping("/save")
	@ApiOperationSupport(order = 4)
	@ApiOperation(value = "新增", notes = "传入info")
	public R save(@Valid @RequestBody Info info) {
		return R.status(infoService.save(info));
	}

	/**
	 * 修改 项目基础信息表
	 */
	@PostMapping("/update")
	@ApiOperationSupport(order = 5)
	@ApiOperation(value = "修改", notes = "传入info")
	public R update(@Valid @RequestBody Info info) {
		return R.status(infoService.updateById(info));
	}

	/**
	 * 新增或修改 项目基础信息表
	 */
	@PostMapping("/submit")
	@ApiOperationSupport(order = 6)
	@ApiOperation(value = "新增或修改", notes = "传入info")
	public R submit(@Valid @RequestBody Info info) {
		return R.status(infoService.saveOrUpdate(info));
	}

	
	/**
	 * 删除 项目基础信息表
	 */
	@PostMapping("/remove")
	@ApiOperationSupport(order = 7)
	@ApiOperation(value = "逻辑删除", notes = "传入ids")
	public R remove(@ApiParam(value = "主键集合", required = true) @RequestParam String ids) {
		return R.status(infoService.deleteLogic(Func.toLongList(ids)));
	}

	
}
