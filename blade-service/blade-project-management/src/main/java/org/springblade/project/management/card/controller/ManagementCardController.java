/*
 *      Copyright (c) 2018-2028, Chill Zhuang All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 *  Neither the name of the dreamlu.net developer nor the names of its
 *  contributors may be used to endorse or promote products derived from
 *  this software without specific prior written permission.
 *  Author: Chill 庄骞 (smallchill@163.com)
 */
package org.springblade.project.management.card.controller;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import lombok.AllArgsConstructor;
import org.springblade.core.boot.ctrl.BladeController;
import org.springblade.core.mp.support.Condition;
import org.springblade.core.mp.support.Query;
import org.springblade.core.tool.api.R;
import org.springblade.core.tool.utils.Func;
import org.springblade.project.management.card.entity.ManagementCard;
import org.springblade.project.management.card.service.IManagementCardService;
import org.springblade.project.management.card.vo.ManagementCardVO;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 *  控制器
 *
 * @author BladeX
 * @since 2021-04-16
 */
@RestController
@AllArgsConstructor
@RequestMapping("/managementcard")
@Api(value = "", tags = "接口")
public class ManagementCardController extends BladeController {

	private final IManagementCardService managementCardService;

	/**
	 * 详情
	 */
	@GetMapping("/detail")
	@ApiOperationSupport(order = 1)
	@ApiOperation(value = "详情", notes = "传入managementCard")
	public R<ManagementCard> detail(ManagementCard managementCard) {
		ManagementCard detail = managementCardService.getOne(Condition.getQueryWrapper(managementCard));
		return R.data(detail);
	}

	/**
	 * 分页
	 */
	@GetMapping("/list")
	@ApiOperationSupport(order = 2)
	@ApiOperation(value = "分页", notes = "传入managementCard")
	public R<IPage<ManagementCard>> list(ManagementCard managementCard, Query query) {
		IPage<ManagementCard> pages = managementCardService.page(Condition.getPage(query), Condition.getQueryWrapper(managementCard));
		return R.data(pages);
	}

	/**
	 * 自定义分页
	 */
	@GetMapping("/page")
	@ApiOperationSupport(order = 3)
	@ApiOperation(value = "分页", notes = "传入managementCard")
	public R<IPage<ManagementCardVO>> page(ManagementCardVO managementCard, Query query) {
		IPage<ManagementCardVO> pages = managementCardService.selectManagementCardPage(Condition.getPage(query), managementCard);
		return R.data(pages);
	}

	/**
	 * 新增
	 */
	@PostMapping("/save")
	@ApiOperationSupport(order = 4)
	@ApiOperation(value = "新增", notes = "传入managementCard")
	public R save(@Valid @RequestBody ManagementCard managementCard) {
		return R.status(managementCardService.save(managementCard));
	}

	/**
	 * 修改
	 */
	@PostMapping("/update")
	@ApiOperationSupport(order = 5)
	@ApiOperation(value = "修改", notes = "传入managementCard")
	public R update(@Valid @RequestBody ManagementCard managementCard) {
		return R.status(managementCardService.updateById(managementCard));
	}

	/**
	 * 新增或修改
	 */
	@PostMapping("/submit")
	@ApiOperationSupport(order = 6)
	@ApiOperation(value = "新增或修改", notes = "传入managementCard")
	public R submit(@Valid @RequestBody ManagementCard managementCard) {
		return R.status(managementCardService.saveOrUpdate(managementCard));
	}


	/**
	 * 删除
	 */
	@PostMapping("/remove")
	@ApiOperationSupport(order = 7)
	@ApiOperation(value = "逻辑删除", notes = "传入ids")
	public R remove(@ApiParam(value = "主键集合", required = true) @RequestParam String ids) {
		return R.status(managementCardService.deleteLogic(Func.toLongList(ids)));
	}


}
