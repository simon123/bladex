/*
 *      Copyright (c) 2018-2028, Chill Zhuang All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 *  Neither the name of the dreamlu.net developer nor the names of its
 *  contributors may be used to endorse or promote products derived from
 *  this software without specific prior written permission.
 *  Author: Chill 庄骞 (smallchill@163.com)
 */
package org.springblade.project.management.card.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import org.springblade.core.mp.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 实体类
 *
 * @author BladeX
 * @since 2021-04-16
 */
@Data
@TableName("project_management_card")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "ManagementCard对象", description = "ManagementCard对象")
public class ManagementCard extends BaseEntity {

	private static final long serialVersionUID = 1L;

	/**
	* 父级菜单
	*/
		@ApiModelProperty(value = "父级菜单")
		private Long parentId;
	/**
	* 菜单编号
	*/
		@ApiModelProperty(value = "菜单编号")
		private String code;
	/**
	* 菜单名称
	*/
		@ApiModelProperty(value = "菜单名称")
		private String name;
	/**
	* 菜单别名
	*/
		@ApiModelProperty(value = "菜单别名")
		private String alias;
	/**
	* 请求地址
	*/
		@ApiModelProperty(value = "请求地址")
		private String path;
	/**
	* 菜单资源
	*/
		@ApiModelProperty(value = "菜单资源")
		private String source;
	/**
	* 排序
	*/
		@ApiModelProperty(value = "排序")
		private Integer sort;
	/**
	* 菜单类型
	*/
		@ApiModelProperty(value = "菜单类型")
		private Integer category;
	/**
	* 操作按钮类型
	*/
		@ApiModelProperty(value = "操作按钮类型")
		private Integer action;
	/**
	* 是否打开新页面
	*/
		@ApiModelProperty(value = "是否打开新页面")
		private Integer isOpen;
	/**
	* 备注
	*/
		@ApiModelProperty(value = "备注")
		private String remark;
	/**
	* 项目id
	*/
		@ApiModelProperty(value = "项目id")
		private Long projectId;


}
