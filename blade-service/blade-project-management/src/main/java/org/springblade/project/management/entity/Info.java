/*
 *      Copyright (c) 2018-2028, Chill Zhuang All rights reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *
 *  Redistributions of source code must retain the above copyright notice,
 *  this list of conditions and the following disclaimer.
 *  Redistributions in binary form must reproduce the above copyright
 *  notice, this list of conditions and the following disclaimer in the
 *  documentation and/or other materials provided with the distribution.
 *  Neither the name of the dreamlu.net developer nor the names of its
 *  contributors may be used to endorse or promote products derived from
 *  this software without specific prior written permission.
 *  Author: Chill 庄骞 (smallchill@163.com)
 */
package org.springblade.project.management.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import org.springblade.core.mp.base.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * 项目基础信息表实体类
 *
 * @author BladeX
 * @since 2021-04-12
 */
@Data
@TableName("project_info")
@EqualsAndHashCode(callSuper = true)
@ApiModel(value = "Info对象", description = "项目基础信息表")
public class Info extends BaseEntity {

	private static final long serialVersionUID = 1L;

	/**
	* 是否固定
	*/
		@ApiModelProperty(value = "是否固定")
		private Integer isFixed;
	/**
	* 项目编号
	*/
		@ApiModelProperty(value = "项目编号")
		private String code;
	/**
	* 项目名称
	*/
		@ApiModelProperty(value = "项目名称")
		private String name;
	/**
	* 项目类型
	*/
		@ApiModelProperty(value = "项目类型")
		private String type;
	/**
	* 年度
	*/
		@ApiModelProperty(value = "年度")
		private String year;
	/**
	* 是否按目标完成
	*/
		@ApiModelProperty(value = "是否按目标完成")
		private String onTarget;
	/**
	* 项目法人
	*/
		@ApiModelProperty(value = "项目法人")
		private String legalPerson;
	/**
	* 项目管理公司
	*/
		@ApiModelProperty(value = "项目管理公司")
		private String managementCompany;
	/**
	* 资金来源
	*/
		@ApiModelProperty(value = "资金来源")
		private String sourceFunds;
	/**
	* 规划条件
	*/
		@ApiModelProperty(value = "规划条件")
		private String planningConditions;
	/**
	* 立项手续
	*/
		@ApiModelProperty(value = "立项手续")
		private String approvalProcedures;
	/**
	* 方案稳定
	*/
		@ApiModelProperty(value = "方案稳定")
		private String stable;
	/**
	* 项目属性
	*/
		@ApiModelProperty(value = "项目属性")
		private String properties;
	/**
	* 项目地址
	*/
		@ApiModelProperty(value = "项目地址")
		private String address;
	/**
	* 项目性质
	*/
		@ApiModelProperty(value = "项目性质")
		private String nature;
	/**
	* 四置
	*/
		@ApiModelProperty(value = "四置")
		private String electronicFence;
	/**
	* 行政区
	*/
		@ApiModelProperty(value = "行政区")
		private String administrativeRegion;
	/**
	* 工程概况
	*/
		@ApiModelProperty(value = "工程概况")
		private String overview;
	/**
	* 存在问题类型
	*/
		@ApiModelProperty(value = "存在问题类型")
		private String problemsType;
	/**
	* 存在问题
	*/
		@ApiModelProperty(value = "存在问题")
		private String problems;
	/**
	* 年度建设目标
	*/
		@ApiModelProperty(value = "年度建设目标")
		private String annualConstructionObjectives;
	/**
	* 年度计划
	*/
		@ApiModelProperty(value = "年度计划")
		private String annualPlan;
	/**
	* 进展
	*/
		@ApiModelProperty(value = "进展")
		private String progress;
	/**
	* 建设内容及规模
	*/
		@ApiModelProperty(value = "建设内容及规模")
		private String contentScale;
	/**
	* 备注
	*/
		@ApiModelProperty(value = "备注")
		private String remark;
	/**
	* 存在风险类型
	*/
		@ApiModelProperty(value = "存在风险类型")
		private String causesRisksType;
	/**
	* 存在风险原因
	*/
		@ApiModelProperty(value = "存在风险原因")
		private String causesRisks;
	/**
	* 不稳定信息关联id
	*/
		@ApiModelProperty(value = "不稳定信息关联id")
		private Long instableId;
	/**
	* 稳定信息关联id
	*/
		@ApiModelProperty(value = "稳定信息关联id")
		private Long stableId;
	/**
	* 项目投资额信息id
	*/
		@ApiModelProperty(value = "项目投资额信息id")
		private Long investmentId;
	/**
	* 参与建设单位信息id
	*/
		@ApiModelProperty(value = "参与建设单位信息id")
		private Long participatingUnitsId;


}
